﻿using UnityEngine;
using System.Collections;

public class RandomForce : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().AddForce(new Vector3(2000 + Random.value * 1000, Random.value * 1000, Random.value * 1000));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
