﻿using UnityEngine;
using System.Collections;

public class VelocityMaintainer : MonoBehaviour {

    public float maxVelocity;
    public bool autoVecocity;

    public float maxAngVelocity;
    public bool autoAngVelocity;
    new Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 currentVel = rigidbody.velocity;
        float currentMag = currentVel.magnitude;
        if (autoVecocity && maxVelocity < currentMag )
        {
            maxVelocity = currentMag;
        }
        rigidbody.velocity = currentVel.normalized * maxVelocity;

        Vector3 currentAngVel = rigidbody.angularVelocity;
        float currentAngMag = currentAngVel.magnitude;
        if (autoAngVelocity && maxAngVelocity < currentAngMag)
        {
            maxAngVelocity = currentAngMag;
        }
        rigidbody.angularVelocity = currentAngVel.normalized * maxAngVelocity;

    }
}
